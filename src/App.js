import "./App.css";

import rowDataApi from "./api/rowDataApi.js";
import React, { useEffect, useState } from "react";
import moment from "moment";

function App() {
  let originCdataList = [];
  let selectCdata = [];
  let toDay = moment().format("YYYY-MM-DD");
  const [selectDate, setSelectDate] = useState(undefined);
  const [lastDataDay, setLastDataDay] = useState(undefined);
  const [deleteDate, setDeleteDate] = useState();
  // const [deleteComplate, setDeleteComplate] = useState(true);
  // const [sendComplate, setSendComplate] = useState(true);
  const [isTypes, setCdataByType] = useState(false);
  const [sendReady, setSendReady] = useState(false);
  const [lastShotList, setLastShotlist] = useState();
  const [lastCtList, setLastCtlist] = useState();
  const [dayRange] = useState(1);
  const [cdatas, setCdata] = useState([]);
  const [lastCdatas, setLastCdatas] = useState([]);
  const [sendCdatas, setSendCdatas] = useState([]);
  //카운터 아이디 검색
  const [counterId] = useState("");
  const [sendResultSearchInfo, setSendResultSearchInfo] = useState({
    terminalId: "",
    start: "",
    end: "",
  });
  const [sendCdataResultNumber, setSendCdataResultNumber] = useState();

  let shotList = [];
  let ctList = [];

  //마지막 cdata(shot, 수신시간 ct) 조회
  useEffect(() => {
    initData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const initData = (byType) => {
    setCdataByType(byType);
    rowDataApi.getLastCdata().then((res) => {
      if (res && res.data) {
        setLastCdatas(res.data);
        res.data.forEach((r) => {
          if (r.shotCount) {
            shotList.push(r.shotCount);
          }
          if (r.cycleTime) {
            ctList.push(r.cycleTime);
          }
        });

        setLastShotlist(shotList);
        setLastCtlist(ctList);

        let srAt = moment(res.data[0].shotReceivedAt, "YYYYMMDDHHmmss")
          .add(1, "days")
          .format("YYYY-MM-DD");

        setSelectDate(srAt);
        setLastDataDay(srAt);

        console.log(
          "선택일자: ",
          selectDate,
          ", 오늘일자: ",
          toDay,
          "오늘 보다 이후인가? ",
          moment(selectDate).isAfter(toDay)
        );
      }
    });
  };

  const changeByType = () => {
    setCdataByType(true);
  };

  //패킷 생성
  const createCdata = () => {
    if (selectDate === undefined) {
      alert("날짜 정보를 입력하세요");
      return;
    }

    let params = {
      params: {
        shotlist: encodeURI(lastShotList),
        ctlist: encodeURI(lastCtList),
      },
    };

    rowDataApi
      .getCdata(selectDate.replace(/-/gi, ""), dayRange, params)
      .then((res) => {
        if (res && res.data) {
          setSendReady(true);
          setCdata(res.data);
          setSendCdatas(null);
          setSendCdataResultNumber(0);
          originCdataList = res.data;
          if (originCdataList.length > 0) {
            setSendResultSearchInfo({
              ...sendResultSearchInfo,
              terminalId: originCdataList[0].terminalId,
              start: originCdataList[0].shotReceivedAt,
              end: originCdataList[originCdataList.length - 1].shotReceivedAt,
            });
          }
        }
      });
  };

  // 패킷 전송
  const sendRowDatas = () => {
    setSendCdataResultNumber(0);
    rowDataApi.sendRowDatas(cdatas).then((res) => {
      if (res && res.data) {
        initData(true);
        setSendCdataResultNumber(res.data.length);
        setSendCdatas(res.data);
        setSendReady(false);
        // setSendComplate(!sendComplate);
      }
    });
  };

  // 패킷 삭제
  const deleteRowDatas = () => {
    if (deleteDate === undefined) {
      alert("패킷을 삭제할 시작일자를 선택해 주세요.");
      return;
    }
    rowDataApi
      .deleteRawDataByDate(
        deleteDate.replace(/-/gi, "") + "000000",
        toDay.replace(/-/gi, "") + "235959"
      )
      .then(() => {
        // setDeleteComplate(!deleteComplate);
        initData();
        setSendCdataResultNumber(0);
        setSendCdatas(null);
        setCdata([]);
      });
  };

  return (
    <div>
      <div className="top_area">
        <span>현재일자: {toDay}</span>
        <div>
          {!moment(selectDate).isAfter(toDay) && (
            <input
              type="date"
              value={selectDate || ""}
              onChange={(e) => {
                setSelectDate(e.target.value);
              }}
              max={toDay}
              min={lastDataDay}
            />
          )}
          {moment(selectDate).isAfter(toDay) && (
            <div className="warning">
              더 이상 패킷을 생성할 수 없습니다. 데이터를 삭제 해주세요.
            </div>
          )}
          {!moment(selectDate).isAfter(toDay) && (
            <button onClick={createCdata}>패킷 생성</button>
          )}
          {sendReady && <button onClick={sendRowDatas}>패킷 전송</button>}
        </div>
        <div>
          <input
            type="date"
            value={deleteDate || ""}
            onChange={(e) => {
              setDeleteDate(e.target.value);
            }}
            max={toDay}
          />
          ~
          <input type="date" value={toDay} disabled />
          <button onClick={deleteRowDatas}>데이터 삭제</button>
        </div>
      </div>
      <div className="middle_area">
        <div className="packet_info_area">
          <div className="title">최종 카운터 정보</div>
          <table>
            <thead>
              <tr>
                <th>No.</th>
                <th>카운터</th>
                <th>SHOT</th>
                <th>수신시간</th>
                <th>C/T</th>
              </tr>
            </thead>
            <tbody>
              {lastCdatas
                // eslint-disable-next-line array-callback-return
                .filter((cdata) => {
                  if (counterId === "") {
                    selectCdata = [];
                    return cdata;
                  } else if (
                    cdata.counterId
                      .toLowerCase()
                      .includes(counterId.toLowerCase())
                  ) {
                    selectCdata.push(cdata);
                    return cdata;
                  }
                })
                .map((data, key) => {
                  return (
                    <tr key={key}>
                      <td>{key + 1}</td>
                      <td>{data.counterId}</td>
                      <td>{data.shotCount}</td>
                      <td>
                        {moment(data.shotReceivedAt, "YYYYMMDDHHmmss").format(
                          "YYYY-MM-DD HH:mm:ss"
                        )}
                      </td>
                      <td>{data.cycleTime}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>

        <div className="send_result_area">
          <div className="send_data_area">
            <div className="title">
              <span>
                생성 패킷 수: {cdatas.length > 0 ? cdatas.length : "0"}
              </span>
              <span style={{ padding: "0 15px;" }}> &gt;&gt; </span>
              <span
                style={{ color: "blue", cursor: "pointer", backgroundColor: "#ffe4c4" }}
                onClick={changeByType}
              >
                [통신 유형별 패킷 변경]
              </span>
            </div>
            <div className="send_table">
              <table>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>카운터</th>
                    <th>SHOT</th>
                    <th>최종샷시간</th>
                    <th>수신시간</th>
                    <th>C/T</th>
                    <th>통신유형</th>
                    <th>배터리</th>
                    <th>C/T 0</th>
                    <th>SHOT 0</th>
                    <th>C/T 1</th>
                    <th>SHOT 1</th>
                    <th>C/T 2</th>
                    <th>SHOT 2</th>
                    <th>C/T 3</th>
                    <th>SHOT 3</th>
                    <th>C/T 4</th>
                    <th>SHOT 4</th>
                    <th>C/T 5</th>
                    <th>SHOT 5</th>
                    <th>C/T 6</th>
                    <th>SHOT 6</th>
                    <th>C/T 7</th>
                    <th>SHOT 7</th>
                    <th>C/T 8</th>
                    <th>SHOT 8</th>
                    <th>C/T 9</th>
                    <th>SHOT 9</th>
                    <th>온도1</th>
                    <th>온도1(시각)</th>
                    <th>온도2</th>
                    <th>온도3</th>
                    <th>온도4</th>
                    <th>온도5</th>
                    <th>현재온도</th>
                    <th>제거횟수</th>
                    <th>누적제거시간(초)</th>
                    <th>최초 제거시각</th>
                    <th>마지막 부착시각</th>
                    <th>부착유무</th>
                    <th>충격횟수</th>
                    <th>외부전원입력</th>
                    <th>터미널</th>
                  </tr>
                </thead>
                <tbody>
                  {cdatas &&
                    cdatas.map((sendCdata, key) => {
                      sendCdata.commType = isTypes && key % 3 === 0 ? "M" : "N";
                      return (
                        <tr key={key}>
                          <td>{key + 1}</td>
                          <td>{sendCdata.counterId}</td>
                          <td>{sendCdata.shotCount}</td>
                          <td>
                            {moment(
                              sendCdata.shotLastAt,
                              "YYYYMMDDHHmmss"
                            ).format("YYYY-MM-DD HH:mm:ss")}
                          </td>
                          <td>
                            {moment(
                              sendCdata.shotReceivedAt,
                              "YYYYMMDDHHmmss"
                            ).format("YYYY-MM-DD HH:mm:ss")}
                          </td>
                          <td>{sendCdata.cycleTime}</td>
                          <td>
                            {sendCdata.commType === "N" ? "정기" : "수동"}
                          </td>
                          <td>{sendCdata.batteryState}</td>
                          <td>{sendCdata.ct0}</td>
                          <td>{sendCdata.shot0}</td>
                          <td>{sendCdata.ct1}</td>
                          <td>{sendCdata.shot1}</td>
                          <td>{sendCdata.ct2}</td>
                          <td>{sendCdata.shot2}</td>
                          <td>{sendCdata.ct3}</td>
                          <td>{sendCdata.shot3}</td>
                          <td>{sendCdata.ct4}</td>
                          <td>{sendCdata.shot4}</td>
                          <td>{sendCdata.ct5}</td>
                          <td>{sendCdata.shot5}</td>
                          <td>{sendCdata.ct6}</td>
                          <td>{sendCdata.shot6}</td>
                          <td>{sendCdata.ct7}</td>
                          <td>{sendCdata.shot7}</td>
                          <td>{sendCdata.ct8}</td>
                          <td>{sendCdata.shot8}</td>
                          <td>{sendCdata.ct9}</td>
                          <td>{sendCdata.shot9}</td>
                          <td>{sendCdata.temporary1}</td>
                          <td>{sendCdata.temporaryFirstAt}</td>
                          <td>{sendCdata.temporary2}</td>
                          <td>{sendCdata.temporary3}</td>
                          <td>{sendCdata.temporary4}</td>
                          <td>{sendCdata.temporary5}</td>
                          <td>{sendCdata.temporaryCurrent}</td>
                          <td>{sendCdata.rmiCount}</td>
                          <td>{sendCdata.rmiAccumTime}</td>
                          <td>{sendCdata.rmiFirstDetachedAt}</td>
                          <td>{sendCdata.rmiLastAttachedAt}</td>
                          <td>{sendCdata.rmiHasAttached}</td>
                          <td>{sendCdata.eiShockCount}</td>
                          <td>{sendCdata.eiHasExternalPower}</td>
                          <td>{sendCdata.terminalId}</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>
          <div className="result_data_area">
            <div className="title">저장 패킷 수: {sendCdataResultNumber}</div>
            <div className="result_table">
              <table>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>카운터</th>
                    <th>SHOT</th>
                    <th>최종샷시간</th>
                    <th>수신시간</th>
                    <th>C/T</th>
                    <th>통신유형</th>
                    <th>배터리</th>
                    <th>C/T 0</th>
                    <th>SHOT 0</th>
                    <th>C/T 1</th>
                    <th>SHOT 1</th>
                    <th>C/T 2</th>
                    <th>SHOT 2</th>
                    <th>C/T 3</th>
                    <th>SHOT 3</th>
                    <th>C/T 4</th>
                    <th>SHOT 4</th>
                    <th>C/T 5</th>
                    <th>SHOT 5</th>
                    <th>C/T 6</th>
                    <th>SHOT 6</th>
                    <th>C/T 7</th>
                    <th>SHOT 7</th>
                    <th>C/T 8</th>
                    <th>SHOT 8</th>
                    <th>C/T 9</th>
                    <th>SHOT 9</th>
                    <th>온도1</th>
                    <th>온도1(시각)</th>
                    <th>온도2</th>
                    <th>온도3</th>
                    <th>온도4</th>
                    <th>온도5</th>
                    <th>현재온도</th>
                    <th>제거횟수</th>
                    <th>누적제거시간(초)</th>
                    <th>최초 제거시각</th>
                    <th>마지막 부착시각</th>
                    <th>부착유무</th>
                    <th>충격횟수</th>
                    <th>외부전원입력</th>
                    <th>터미널</th>
                  </tr>
                </thead>
                <tbody>
                  {sendCdatas &&
                    sendCdatas.map((resultData, key) => {
                      return (
                        <tr key={key}>
                          <td>{key + 1}</td>
                          <td>{resultData.counterId}</td>
                          <td>{resultData.shotCount}</td>
                          <td>
                            {moment(
                              resultData.shotLastAt,
                              "YYYYMMDDHHmmss"
                            ).format("YYYY-MM-DD HH:mm:ss")}
                          </td>
                          <td>
                            {moment(
                              resultData.shotReceivedAt,
                              "YYYYMMDDHHmmss"
                            ).format("YYYY-MM-DD HH:mm:ss")}
                          </td>
                          <td>{resultData.cycleTime}</td>
                          <td>
                            {resultData.commType === "N" ? "정기" : "수동"}
                          </td>
                          <td>{resultData.batteryState}</td>
                          <td>{resultData.ct0}</td>
                          <td>{resultData.shot0}</td>
                          <td>{resultData.ct1}</td>
                          <td>{resultData.shot1}</td>
                          <td>{resultData.ct2}</td>
                          <td>{resultData.shot2}</td>
                          <td>{resultData.ct3}</td>
                          <td>{resultData.shot3}</td>
                          <td>{resultData.ct4}</td>
                          <td>{resultData.shot4}</td>
                          <td>{resultData.ct5}</td>
                          <td>{resultData.shot5}</td>
                          <td>{resultData.ct6}</td>
                          <td>{resultData.shot6}</td>
                          <td>{resultData.ct7}</td>
                          <td>{resultData.shot7}</td>
                          <td>{resultData.ct8}</td>
                          <td>{resultData.shot8}</td>
                          <td>{resultData.ct9}</td>
                          <td>{resultData.shot9}</td>
                          <td>{resultData.temporary1}</td>
                          <td>{resultData.temporaryFirstAt}</td>
                          <td>{resultData.temporary2}</td>
                          <td>{resultData.temporary3}</td>
                          <td>{resultData.temporary4}</td>
                          <td>{resultData.temporary5}</td>
                          <td>{resultData.temporaryCurrent}</td>
                          <td>{resultData.rmiCount}</td>
                          <td>{resultData.rmiAccumTime}</td>
                          <td>{resultData.rmiFirstDetachedAt}</td>
                          <td>{resultData.rmiLastAttachedAt}</td>
                          <td>{resultData.rmiHasAttached}</td>
                          <td>{resultData.eiShockCount}</td>
                          <td>{resultData.eiHasExternalPower}</td>
                          <td>{resultData.terminalId}</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
