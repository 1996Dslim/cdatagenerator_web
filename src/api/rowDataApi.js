import service from "../service/request.js";

const PRODUCT_API_BASE_URL = process.env.REACT_APP_API_URL;

class TestApi {
  getLastCdata() {
    return service.get(PRODUCT_API_BASE_URL + "/lastcdata");
  }

  getCdata(selectDate, dayRange, params) {
    console.log(params.params.ctlist);
    return service.get(
      PRODUCT_API_BASE_URL + "/cdata/" + selectDate + "/" + dayRange,
      params
    );
  }

  decryptRowData() {
    return service.get(PRODUCT_API_BASE_URL + "/decryptrowdata");
  }

  sendRowDatas(rowDatas) {
    return service.post(PRODUCT_API_BASE_URL + "/rawdatas", rowDatas);
  }

  getByTerminalId(terminalId, params) {
    return service.get(
      PRODUCT_API_BASE_URL + "/terminal/" + terminalId + "/rawdata",
      params
    );
  }

  deleteRawDataByDate(start, end) {
    console.log(start, end);
    return service.delete(
      PRODUCT_API_BASE_URL + "/rawdatas/" + start + "/" + end
    );
  }
}

export default new TestApi();
